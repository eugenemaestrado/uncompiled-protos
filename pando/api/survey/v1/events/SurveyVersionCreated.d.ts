import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyVersionCreated {
    'versionId'?: (string);
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyVersionCreated__Output {
    'versionId'?: (string);
    'surveyId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
