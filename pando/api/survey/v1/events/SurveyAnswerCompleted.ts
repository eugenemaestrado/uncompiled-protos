// Original file: proto/pando/api/survey/v1/events.proto

import type { CompletionType as _pando_api_survey_v1_CompletionType } from '../../../../../pando/api/survey/v1/CompletionType';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface SurveyAnswerCompleted {
  'answerId'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType | keyof typeof _pando_api_survey_v1_CompletionType);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface SurveyAnswerCompleted__Output {
  'answerId'?: (string);
  'completionType'?: (_pando_api_survey_v1_CompletionType);
  'userData'?: (_pando_api_UserMetadata__Output);
}
