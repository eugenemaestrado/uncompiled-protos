import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyMediaDeleted {
    'surveyId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyMediaDeleted__Output {
    'surveyId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
