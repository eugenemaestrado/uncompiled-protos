import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyMediaAccessed {
    'surveyId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyMediaAccessed__Output {
    'surveyId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
