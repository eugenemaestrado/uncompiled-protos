import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyAnswerMediaUploaded {
    'answerId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyAnswerMediaUploaded__Output {
    'answerId'?: (string);
    'mediaId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
