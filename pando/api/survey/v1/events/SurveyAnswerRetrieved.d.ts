import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyAnswerRetrieved {
    'answerId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyAnswerRetrieved__Output {
    'answerId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
