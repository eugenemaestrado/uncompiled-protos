import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface InstanceESignSignatoryDeleted {
    'instanceId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface InstanceESignSignatoryDeleted__Output {
    'instanceId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
