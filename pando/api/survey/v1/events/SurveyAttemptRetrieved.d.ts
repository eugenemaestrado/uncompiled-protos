import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyAttemptRetrieved {
    'attemptId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyAttemptRetrieved__Output {
    'attemptId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
