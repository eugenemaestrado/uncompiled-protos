// Original file: proto/pando/api/survey/v1/events.proto

import type { SurveyQuestionDestination as _pando_api_survey_v1_SurveyQuestionDestination, SurveyQuestionDestination__Output as _pando_api_survey_v1_SurveyQuestionDestination__Output } from '../../../../../pando/api/survey/v1/SurveyQuestionDestination';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface SurveyQuestionDestinationChanged {
  'surveyQuestionDestinationId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_SurveyQuestionDestination | null);
  'newModel'?: (_pando_api_survey_v1_SurveyQuestionDestination | null);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface SurveyQuestionDestinationChanged__Output {
  'surveyQuestionDestinationId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_SurveyQuestionDestination__Output);
  'newModel'?: (_pando_api_survey_v1_SurveyQuestionDestination__Output);
  'userData'?: (_pando_api_UserMetadata__Output);
}
