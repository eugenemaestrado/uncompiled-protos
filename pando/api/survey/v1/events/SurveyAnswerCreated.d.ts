import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyAnswerCreated {
    'answerId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyAnswerCreated__Output {
    'answerId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
