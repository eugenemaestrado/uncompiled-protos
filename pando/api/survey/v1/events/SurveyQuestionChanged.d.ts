import type { Question as _pando_api_survey_v1_Question, Question__Output as _pando_api_survey_v1_Question__Output } from '../../../../../pando/api/survey/v1/Question';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface SurveyQuestionChanged {
    'questionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_Question | null);
    'newModel'?: (_pando_api_survey_v1_Question | null);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface SurveyQuestionChanged__Output {
    'questionId'?: (string);
    'oldModel'?: (_pando_api_survey_v1_Question__Output);
    'newModel'?: (_pando_api_survey_v1_Question__Output);
    'userData'?: (_pando_api_UserMetadata__Output);
}
