import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';
export interface AnswerOptionCreated {
    'answerOptionId'?: (string);
    'userData'?: (_pando_api_UserMetadata | null);
}
export interface AnswerOptionCreated__Output {
    'answerOptionId'?: (string);
    'userData'?: (_pando_api_UserMetadata__Output);
}
