// Original file: proto/pando/api/survey/v1/events.proto

import type { Variable as _pando_api_survey_v1_Variable, Variable__Output as _pando_api_survey_v1_Variable__Output } from '../../../../../pando/api/survey/v1/Variable';
import type { UserMetadata as _pando_api_UserMetadata, UserMetadata__Output as _pando_api_UserMetadata__Output } from '../../../../../pando/api/UserMetadata';

export interface VariableUpdated {
  'variableId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_Variable | null);
  'newModel'?: (_pando_api_survey_v1_Variable | null);
  'userData'?: (_pando_api_UserMetadata | null);
}

export interface VariableUpdated__Output {
  'variableId'?: (string);
  'oldModel'?: (_pando_api_survey_v1_Variable__Output);
  'newModel'?: (_pando_api_survey_v1_Variable__Output);
  'userData'?: (_pando_api_UserMetadata__Output);
}
